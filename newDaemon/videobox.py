#!/usr/bin/env python3
import os
import sys
import threading
from pathlib import Path            # used by omxplayyer-wrapper
import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522
import pygame
import time
import subprocess
import signal
import configparser
import mpd
from omxplayer.player import OMXPlayer
import json

reader = SimpleMFRC522()    # initialize card reader object
clock = pygame.time.Clock() # set the game clock. 

configfile = "/etc/videobox/vboxconfig.ini"
config = configparser.ConfigParser() 
config.read(configfile)

music_client = mpd.MPDClient()
music_client.connect("localhost", 6600)

crashed = False

def handler_stop_signals(signum, frame):
    global crashed
    crashed = True

signal.signal(signal.SIGINT, handler_stop_signals)
signal.signal(signal.SIGTERM, handler_stop_signals)     

class vboxdaemon:
    workdir = "/var/lib/videobox"
    screen = None;
    prev_id = None;
    status = "Starting up"
    prevStatus = "Starting up"
    playlist = "notYetMapped"
    playlist_prev = "test playlist old"
    trackElapsed = "0"
    trackPlaylistPosition = "0"
    Playing = False
    image = pygame.image.load('/usr/local/share/images/videobox/fallback.png')
    id = None;
    
    def __init__(self):
        "Ininitializes a new pygame screen using the framebuffer"
        # Based on "Python GUI in Linux frame buffer"
        # http://www.karoltomala.com/blog/?p=679
        disp_no = os.getenv("DISPLAY")
        if disp_no:
            print("I'm running under X display = {0}", format(disp_no))
        
        # Check which frame buffer drivers are available
        # Start with fbcon since directfb hangs with composite output
        drivers = ['fbcon', 'directfb', 'svgalib']
        found = False
        for driver in drivers:
            # Make sure that SDL_VIDEODRIVER is set
            if not os.getenv('SDL_VIDEODRIVER'):
                os.putenv('SDL_VIDEODRIVER', driver)
            try:
                pygame.display.init()
            except pygame.error:
                print("Driver: {0} failed.", format(driver))
                continue
            found = True
            break
    
        if not found:
            raise Exception('No suitable video driver found!')
        
        size = (pygame.display.Info().current_w, pygame.display.Info().current_h)
        print("Framebuffer size: ", size[0], " x ", size[1], ".")
        global screen_w
        global screen_h
        screen_w = size[0]
        screen_h = size[1]
        self.screen = pygame.display.set_mode(size, pygame.FULLSCREEN)
        # Clear the screen to start
        self.screen.fill((0, 0, 0))        
        pygame.mouse.set_visible(False)
        # Render the screen
        pygame.display.update()

    def drawbg(self):
        # Fill the screen with bgcolor 
        bgcolor = (2, 80, 88)
        self.screen.fill(bgcolor)
        iw, ih = self.image.get_size()
        if iw >= screen_w:
            bx = 0
        else:
            bx = (screen_w - iw) / 2
        if ih >= screen_h:
            by = 0
        else:
            by = (screen_h - ih) / 2
        self.screen.blit(self.image,(bx,by))
        # Update the display
        pygame.display.update()

    def playVideo(self, playlist):
        filePath = self.workdir + "/" + os.path.basename(playlist)
        try:
            with open(filePath) as json_file:
              data = json.load(json_file)
              video_pos = float(data["video_pos"])
              print("Resuming %s at second %s." % (playlist, video_pos))
        except: 
            print("Could not read stored statefile %s" % (filePath))
            print("Starting to play video from beginning.")
            video_pos = 0
        videopath=Path(playlist)
        self.video_client = OMXPlayer(videopath, args=['-o', 'alsa'])
        self.video_client.load(videopath, pause=True)
        duration = self.video_client.duration()
        if video_pos > duration:            # make sure we do not search beyond file end.
           video_pos = 0
        self.video_client.seek(0)           # seek is relative, so we have 
        self.video_client.seek(video_pos)   # to jump to start for an absolute position.
        self.video_client.play()

    def playMusic(self, playlist):
        filePath = self.workdir + "/" + playlist
        try:
            with open(filePath) as json_file:
              data = json.load(json_file)
              playlist_pos = data["playlist_pos"]
              song_pos = data["song_pos"]
              print("Resuming from playlist position %s, at second %s." % (playlist_pos, song_pos))
        except: 
            print("Could not read stored statefile %s" % (filePath))
            print("Starting to play music from beginning.")
            playlist_pos = "0" 
            song_pos = "0"
        music_client.clear()
        try:
            try:
                music_client.load(playlist)
            except:
                music_client.load("notYetMapped")
            music_client.seek(playlist_pos, song_pos)
            music_client.play
        except:
            print("Could not load playlist %s" % (playlist))

    def startSlideshow(self, imageDir):
        self.Playing = True
        print("Starting to show picures")
        # Load image names from a given directory into an arry
        fileDirContent = os.listdir(imageDir)
        counterMax = len(fileDirContent)
        print("Files in dir " + imageDir + ":" + str(counterMax))
        counter = 1
        while self.status == "slideshow":
          loadPath = imageDir + "/" + fileDirContent[counter]
          self.image = pygame.image.load(loadPath) 
          print("Try to load image " + loadPath) 
          bx = 1200
          by = 700
          ix,iy = self.image.get_size()
          if ix > iy:
            # fit to width
            scale_factor = bx/float(ix)
            sy = scale_factor * iy
            if sy > by:
                scale_factor = by/float(iy)
                sx = scale_factor * ix
                sy = by
            else:
                sx = bx
          else:
            # fit to height
            scale_factor = by/float(iy)
            sx = scale_factor * ix
            if sx > bx:
                scale_factor = bx/float(ix)
                sx = bx
                sy = scale_factor * iy
            else:
                sy = by
          self.image = pygame.transform.scale(self.image, (int(sx), int(sy)))
          time.sleep(4.0)
          if counter > counterMax - 1:
              counter = 1
          else:
              counter += 1
        print ("Exiting slidesow thread.")
        self.Playing = False


    def mapCardToPlaylist(self, uid):
        print("Try to lookup playlist for uid " + uid)
        sections = config.sections()
        print("Found the following sections in config file: " + str(sections))
        try:
            self.status in sections
        except:
            print("Adding section to config file: " + self.status)
            config[self.status] = {}
            with open(configfile, 'w') as configfile_w:
                config.write(configfile_w)
                configfile_w.close()
        try:
            # lookup if uid exists in config file
            self.playlist_prev = self.playlist
            self.playlist = config.get(self.status, uid)
            return self.playlist
        except:
            print("NFC chip is not mapped in config: " + uid)
            config[self.status][uid] = 'notYetMapped'
            with open(configfile, 'w') as configfile_w:
                config.write(configfile_w)
                configfile_w.close()
            return "noCard"

    def checkCard(self):
        time.sleep(0.5)
        read_error_count = 0
        id = None
        while id == None:
          time.sleep(0.1)
          id, text = reader.read_no_block()
          read_error_count += 1
          # I don't know why every 2nd attempt fails
          if read_error_count > 2: 
            self.status = "nocard"
            break
        if id != self.prev_id:
          print("Something has changed")
          mystring =  text or "nocard"
          hexxless = mystring.replace('\x00', '')
          self.status = hexxless.replace(" ", "")
          playlist = self.mapCardToPlaylist(str(id))
          print("New playlist: " + playlist)
          print("ID: %s\tStatus: %s" % (id,self.status))
          self.prev_id = id

    def evalStatus(self):
        if self.status != self.prevStatus:
            print("eval status \"%s\" vs prev. status %s" % (self.status, self.prevStatus))
            if self.status == "nocard":
              if self.prevStatus == "call":
                  print("Stopping call")
              if self.prevStatus == "music":
                  print("Stopping music playlist %s on position %s at second %s" % (self.playlist_prev, self.trackPlaylistPosition, self.trackElapsed))
                  music_client.stop()
                  try:
                    filePath = self.workdir + "/" + self.playlist_prev
                    playlistData = {'playlist_name': self.playlist_prev, 'playlist_pos': self.trackPlaylistPosition, 'song_pos': self.trackElapsed}
                    with open(filePath, 'w') as statusfile:
                      json.dump(playlistData, statusfile)
                      statusfile.close
                  except:
                    print("Could not write data to statusfile")
              if self.prevStatus == "video":
                  print("Stopping video")
                  self.video_client.pause()
                  try:
                    filePath = self.workdir + "/" + os.path.basename(self.playlist_prev)
                    playlistData = {'playlist_name': self.playlist_prev, 'video_pos': self.video_client.position()}
                    with open(filePath, 'w') as statusfile:
                      json.dump(playlistData, statusfile)
                      statusfile.close
                  except:
                    print("Could not write data to statusfile")
                  self.video_client.stop()
              if self.prevStatus == "slideshow":
                  print("Stopping slideshow")
              self.image = pygame.image.load('/usr/local/share/images/videobox/stopped.png')
              self.Playing = False
            if self.status == "music":
              self.image = pygame.image.load('/usr/local/share/images/videobox/music.png') 
              self.playMusic(self.playlist)
            if self.status == "video":
              self.image = pygame.image.load('/usr/local/share/images/videobox/video.png') 
              videoThread = threading.Thread(target=self.playVideo, args=(self.playlist,))
              videoThread.start()
            if self.status == "slideshow":
              self.image = pygame.image.load('/usr/local/share/images/videobox/slideshow.png') 
              slideThread = threading.Thread(target=self.startSlideshow, args=(self.playlist,))
              slideThread.start()
            if self.status == "call":
              self.image = pygame.image.load('/usr/local/share/images/videobox/call.png') 
            if self.status == "shutdown":
              subprocess.call(["/sbin/shutdown", "-h", "now"])
        else:
            # if status is something else but "nocard" we activate our dead man switch
            if self.Playing:
              subprocess.call(["/usr/bin/touch", "/var/run/shutdownfile"])
            
            # Keep track of playlist and song position currently playing.
            # This information is written to a file on a clean "music stop" event
            # and then re-loaded on startup to continue a playlist/ song at a certain position
            if self.status == "music":
                try: 
                  mpdStatus = music_client.status()
                  elapsedSeconds = mpdStatus['time']
                  splitString = elapsedSeconds.split(':')
                  newPosition = splitString[0]
                  if newPosition == self.trackElapsed:
                      print("Music stopped.")
                      self.Playing = False
                  else:
                      self.Playing = True
                  self.trackElapsed = newPosition
                  self.trackPlaylistPosition = mpdStatus['song']
                except: 
                  print("Could not read MPD status.")
                  self.trackElapsed = "0" 
                  self.trackPlaylistPosition = 0 
                  self.Playing = False

        self.prevStatus = self.status

              
# Create an instance of the vboxdaemon class
scope = vboxdaemon()
while not crashed:
  scope.checkCard()
  scope.evalStatus()
  scope.drawbg()
  clock.tick(2)     # set frames per second
music_client.stop()
music_client.close()
music_client.disconnect()
pygame.display.quit()
GPIO.cleanup()

