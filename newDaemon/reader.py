from time import sleep
import sys
import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522
reader = SimpleMFRC522()

try:
    while True:
        print("Hold a tag near the reader")
        id, text = reader.read()
        print("ID: %s\nText: %s" % (id,text))
        sleep(1)
        print(repr(text))
        hexlesstext = text.replace('\x00',"")
        mytext = hexlesstext.replace(" ", "")
        print(repr(mytext))
        if mytext == "call":
            print("yes")
        else: 
            print("\"",text,"\"")
except KeyboardInterrupt:
    GPIO.cleanup()
    raise
