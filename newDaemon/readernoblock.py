from time import sleep
import sys
import RPi.GPIO as GPIO
from mfrc522 import SimpleMFRC522
reader = SimpleMFRC522()
id = None
prev_id = None
status = "nocard"
read_error_count = 0
try:
    while True:
        sleep(0.5)
        id = None
        read_error_count = 0
        while id == None:
          sleep(0.1)
          id, text = reader.read_no_block()
          read_error_count += 1
          # I don't know why every 2nd attempt fails
          if read_error_count > 2: 
            status = "nocard"
            break
        if id != prev_id:
          print("Something has changed")
          status = text

        else: 
          print("Nothing has changed")
        print("ID: %s\tStatus: %s" % (id,status))
        prev_id = id

except KeyboardInterrupt:
    GPIO.cleanup()
    raise
