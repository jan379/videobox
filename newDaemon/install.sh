#!/usr/bin/env bash

# install dependencies
apt install -y python3-pygame
apt install -y python3-pip
apt install -y python3-mpd 
pip3 install mfrc522
pip3 install omxplayer-wrapper

# make a working environment
mkdir -p /var/lib/videobox

# create artwork directory
mkdir -p /usr/local/share/images/videobox
cp images/* /usr/local/share/images/videobox/

# install to /usr/local/bin
cp videobox.py /usr/local/bin/videobox.py
chmod 755 /usr/local/bin/videobox.py

# create config dir 
mkdir -p /etc/videobox

# install service file
cp videobox.service /etc/systemd/system/videobox.service
# enable service
systemctl enable videobox.service
# in case that we overwrite an old service file:
systemctl daemon-reload
# start service
systemctl restart videobox.service
systemctl status videobox.service
