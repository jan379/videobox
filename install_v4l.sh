curl https://www.linux-projects.org/listing/uv4l_repo/lpkey.asc | sudo apt-key add -


echo "deb https://www.linux-projects.org/listing/uv4l_repo/raspbian/stretch stretch main" > /etc/apt/sources.list.d/u4vl.list

apt-get update

apt-get install -y uv4l uv4l-raspicam uv4l-raspicam-extras uv4l-server uv4l-webrtc uv4l-demos

 
