#!/usr/bin/env bash

# This program shuts down the box 
# if a file time stamp is older than 
# 30 minutes

testfile=/var/run/shutdownfile

if test -f ${testfile} ; then
	echo "Found test file"
else	touch ${testfile}
fi

# give 30 minutes to the system before shutting down:
touch ${testfile}

while true; do
  date=$(date +%s)
  fileage=$(ls -l --time-style=+%s ${testfile} | cut -d" " -f6)
  fileage_minutes=$(((date - fileage)/60))

  echo "Fileage in minutes: ${fileage_minutes}"

  if [[ ${fileage_minutes} -gt 30 ]]; then
	/sbin/shutdown -h 1
	echo "I will shut down the system in two minutes due to inactivity"
  fi
  sleep 120
done
