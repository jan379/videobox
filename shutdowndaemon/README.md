# Shutdown service

This service shuts down the whole box if it does
not detect a recent time stamp (< 30 minutes) on 
a defined file. 
By default this file is located in /tmp/test.

You can update this time stamp manually if needed:

```
$ touch /tmp/test
```

This is done automatically for example by the "videobox" 
programm if it is playing music or doing other activity.



