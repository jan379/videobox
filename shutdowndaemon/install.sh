#!/usr/bin/env bash

# install to /usr/local/bin
cp shutdowndaemon.sh /usr/local/sbin/shutdowndaemon.sh
chmod 755 /usr/local/sbin/shutdowndaemon.sh
# install service file
cp shutdowndaemon.service /etc/systemd/system/shutdowndaemon.service
# enable service
systemctl enable shutdowndaemon.service
# in case that we overwrite an old service file:
systemctl daemon-reload
# start service
systemctl restart shutdowndaemon.service
systemctl status shutdowndaemon.service
